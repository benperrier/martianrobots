import Model.MarsTerrainMap;
import Model.PlanetMap;
import Model.Robot;

/**
 * Created by accbox on 6/19/2017.
 */
public class Application {
    public static void main( String[] args){
        PlanetMap martianMap = new MarsTerrainMap();

        Robot r2d2 = new Robot(martianMap);
        r2d2.executeInstructions("5 3\n" +
                "1 1 N\n"+
                "FFFFFF\n"
        );
        r2d2.executeInstructions("5 3\n" +
                "1 1 E\n"+
                "RFRFRFRF\n"
        );
        r2d2.executeInstructions("0 3 W\n" +
                "LLFFFLFLFL");


        Robot c3po = new Robot(martianMap);
        c3po.executeInstructions("3 6\n" +
                "1 1 E\n"+
                "RFRFRFRF\n"
        );
        c3po.executeInstructions("0 3 W\n" +
                "LLFFFLFLFL");

    }
}
