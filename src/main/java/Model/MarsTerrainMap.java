package Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by accbox on 6/19/2017.
 */
public class MarsTerrainMap implements PlanetMap {
    String PLANET_NAME = "Mars";

    private int MIN_X = 0;

    private int maxX;

    private int MIN_Y = 0;

    private int maxY;

    public boolean isPositionLost(Position position){
        return position.getX()<MIN_X || position.getX()>maxX || position.getY()<MIN_Y || position.getY()>maxY;
    }
    private List<Position> lostPositions = new ArrayList<Position>();

    public void addLostPositon(Position position){
        this.lostPositions.add(position);
    }

    public int getMaxX() {
        return maxX;
    }

    public void setMaxX(int maxX) {
        this.maxX = maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public void setMaxY(int maxY) {
        this.maxY = maxY;
    }
}
