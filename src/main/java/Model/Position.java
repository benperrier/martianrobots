package Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by accbox on 6/19/2017.
 */
public class Position {

    private int x;
    private int y;
    private String orientation;
    private boolean isLost;
    private Map<String, Position> movements;

    Position(int x, int y, String orientation){
        this.setX(x);
        this.setY(y);
        this.setOrientation(orientation);
        this.setLost(false);

        this.movements = new HashMap<String, Position>();
        this.movements.put("N", new Position(0,1));
        this.movements.put("E", new Position(1,0));
        this.movements.put("S", new Position(0,-1));
        this.movements.put("W", new Position(-1,0));
    }

    Position(int x, int y){
        this.setX(x);
        this.setY(y);
    }

    public void rotate(String direction){
        String newOrientation = "";
        if (this.getOrientation().equals( "N"))  newOrientation = direction.equals("R") ? "E" : "W";
        if (this.getOrientation().equals( "E"))  newOrientation = direction.equals("R") ? "S" : "N";
        if (this.getOrientation().equals( "S"))  newOrientation = direction.equals("R") ? "W" : "E";
        if (this.getOrientation().equals( "W"))  newOrientation = direction.equals("R") ? "N" : "S";
        setOrientation(newOrientation);
    }
    
    public void moveForward(){
        Position relativeNextPosition = (Position)this.movements.get(orientation);
        this.moveToRelativePosition(relativeNextPosition);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void moveToRelativePosition(Position relativeNextPosition) {
        setX(this.x + relativeNextPosition.getX());
        setY(this.y + relativeNextPosition.getY());
    }


    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public boolean isLost() {
        return isLost;
    }

    public void setLost(boolean lost) {
        isLost = lost;
    }
}
