package Model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by accbox on 6/19/2017.
 */
public class Robot {

    private Position currentPosition;

    private PlanetMap planetMap;

    public Robot(PlanetMap planetMap){
        this.planetMap = planetMap;
    }

    public void executeInstructions(String instructionsString){
        List<String> receivedInstuctions = new LinkedList<String>(Arrays.asList(instructionsString.split("\\r\\n|\\n|\\r")));

        if(receivedInstuctions.size()>2){
            // This line corresponds to Planet Map Max size
            List<String> currentPosition = Arrays.asList(receivedInstuctions.get(0).split(" "));
            this.planetMap.setMaxX(Integer.valueOf(currentPosition.get(0)));
            this.planetMap.setMaxY(Integer.valueOf(currentPosition.get(1)));
            receivedInstuctions.remove(0);
        }

        // This line corresponds to the initial position
        List<String> currentCoordinates = Arrays.asList(receivedInstuctions.get(0).split(" "));
        this.currentPosition = new Position(Integer.valueOf(currentCoordinates.get(0)),Integer.valueOf(currentCoordinates.get(1)), currentCoordinates.get(2));
        String instructions = receivedInstuctions.get(1);
        for(String instruction : instructions.split("(?!^)")){
            this.currentPosition.setLost(this.planetMap.isPositionLost(this.currentPosition));
            if(this.currentPosition.isLost()) break;
            if(instruction.equals("R")) currentPosition.rotate("R");
            if(instruction.equals("L")) currentPosition.rotate("L");
            if(instruction.equals("F")) currentPosition.moveForward();
        }

        if(this.currentPosition.isLost()){
            planetMap.addLostPositon(this.currentPosition);
        }
        reportOutput();
    }

    public void reportOutput(){
        String isRobotLost = currentPosition.isLost() ? " LOST":"";
        System.out.println(currentPosition.getX()+" "+currentPosition.getY()+" "+ currentPosition.getOrientation()+isRobotLost);
    }

}
