package Model;

/**
 * Created by accbox on 6/19/2017.
 */
public interface PlanetMap {

    void setMaxX(int maxX);
    void setMaxY(int maxY);

    boolean isPositionLost(Position position);
    void addLostPositon(Position position);


}
